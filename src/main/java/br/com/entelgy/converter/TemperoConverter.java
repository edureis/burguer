package br.com.entelgy.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.hibernate.Session;
import br.com.entelgy.model.Tempero;
import br.com.entelgy.util.HibernateUtil;
@FacesConverter(forClass=Tempero.class)
public class TemperoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Tempero retorno = null;
		if(value != null) {
			Session session = HibernateUtil.getSession();
			retorno = (Tempero) session.get(Tempero.class, new Integer(value));
			session.close();
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Tempero) value).getCodigo().toString();
		}
		return null;
	}

}
