package br.com.entelgy.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.hibernate.Session;

import br.com.entelgy.model.Lanche;
import br.com.entelgy.util.HibernateUtil;

@FacesConverter(forClass=Lanche.class)
public class LancheConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Lanche retorno = null;
		if(value != null){
			Session session = HibernateUtil.getSession();
			retorno = (Lanche) session.get(Lanche.class, new Long(value));
			session.close();
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Lanche) value).getCodigo().toString();
		}
		return null;

	}

}
