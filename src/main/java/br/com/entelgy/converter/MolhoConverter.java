package br.com.entelgy.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.hibernate.Session;

import br.com.entelgy.model.Molho;
import br.com.entelgy.util.HibernateUtil;
@FacesConverter(forClass=Molho.class)
public class MolhoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Molho retorno = null;
		if(value != null) {
			Session session = HibernateUtil.getSession();
			retorno = (Molho) session.get(Molho.class, new Integer(value));
			session.close();
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Molho) value).getCodigo().toString();
		}
		return null;
	}

}
