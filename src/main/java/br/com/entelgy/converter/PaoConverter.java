package br.com.entelgy.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.hibernate.Session;


import br.com.entelgy.model.TipoPao;
import br.com.entelgy.util.HibernateUtil;
@FacesConverter(forClass=TipoPao.class)
public class PaoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		TipoPao retorno = null;
		if(value != null) {
			Session session = HibernateUtil.getSession();
			retorno = (TipoPao) session.get(TipoPao.class, new Integer(value));
			session.close();
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((TipoPao) value).getCodigo().toString();
		}
		return null;
	}

}
