package br.com.entelgy.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.hibernate.Session;
import br.com.entelgy.model.Recheio;
import br.com.entelgy.util.HibernateUtil;
@FacesConverter(forClass=Recheio.class)
public class RecheioConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Recheio retorno = null;
		if(value != null) {
			Session session = HibernateUtil.getSession();
			retorno = (Recheio) session.get(Recheio.class, new Integer(value));
			session.close();
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Recheio) value).getCodigo().toString();
		}
		return null;
	}

}
