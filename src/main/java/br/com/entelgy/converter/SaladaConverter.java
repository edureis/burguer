package br.com.entelgy.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.hibernate.Session;


import br.com.entelgy.model.Salada;
import br.com.entelgy.util.HibernateUtil;
@FacesConverter(forClass=Salada.class)
public class SaladaConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Salada retorno = null;
		if(value != null) {
			Session session = HibernateUtil.getSession();
			retorno = (Salada) session.get(Salada.class, new Integer(value));
			session.close();
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Salada) value).getCodigo().toString();
		}
		return null;
	}

}
