package br.com.entelgy.view;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.entelgy.model.Lanche;
import br.com.entelgy.model.Molho;
import br.com.entelgy.model.Pedido;
import br.com.entelgy.model.Pessoa;
import br.com.entelgy.model.Queijo;
import br.com.entelgy.model.Recheio;
import br.com.entelgy.model.Salada;
import br.com.entelgy.model.Tempero;
import br.com.entelgy.model.TipoPao;
import br.com.entelgy.service.PedidoService;
import br.com.entelgy.util.HibernateUtil;
import br.com.entelgy.util.Repositorios;

@ManagedBean
@ViewScoped
public class PedidoManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<Lanche> lanches = new ArrayList<Lanche>();
	private List<TipoPao> tiposDePao = new ArrayList<TipoPao>();
	private List<Queijo> tiposDeQueijo = new ArrayList<Queijo>();
	private List<Recheio> tiposDeRecheio = new ArrayList<Recheio>();
	private List<Salada> tiposDeSalada = new ArrayList<Salada>();
	private List<Molho> tiposDeMolho = new ArrayList<Molho>();
	private List<Tempero> tiposDeTempero = new ArrayList<Tempero>();
	private Repositorios repositorios = new Repositorios();
	private Pedido pedido = new Pedido();
	private Lanche lanche = new Lanche();
	private TipoPao tipoPao = new TipoPao();
	private Queijo queijo = new Queijo();
	private Recheio recheio = new Recheio();
	private Salada salada = new Salada();
	private Molho molho = new Molho();	
	private Tempero tempero = new Tempero();
	private Pessoa pessoa = new Pessoa();
	


	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init(){
	

		Session session = HibernateUtil.getSession();
		
		this.lanches = session.createCriteria(Lanche.class)
				.addOrder(Order.asc("descricao"))
				.list();
		this.tiposDePao = session.createCriteria(TipoPao.class)
				.addOrder(Order.asc("descricao"))
				.list();
		this.tiposDeQueijo = session.createCriteria(Queijo.class)
				.addOrder(Order.asc("descricao"))
				.list();
		this.tiposDeRecheio = session.createCriteria(Recheio.class)
				.addOrder(Order.asc("descricao"))
				.list();
		this.tiposDeSalada = session.createCriteria(Salada.class)
				.addOrder(Order.asc("descricao"))
				.list();
		this.tiposDeMolho = session.createCriteria(Molho.class)
				.addOrder(Order.asc("descricao"))
				.list();
		this.tiposDeTempero = session.createCriteria(Tempero.class)
				.addOrder(Order.asc("descricao"))
				.list();
		session.close();
		
	}

	public List<TipoPao> getTiposDePao() {
		return tiposDePao;
	}

	public TipoPao getTipoPao() {
		return tipoPao;
	}

	public List<Lanche> getLanches() {
		return lanches;
	}

	public Lanche getLanche() {
		return lanche;
	}
	
	public List<Recheio> getTiposDeRecheio() {
		return tiposDeRecheio;
	}

	public Recheio getRecheio() {
		return recheio;
	}

	public List<Queijo> getTiposDeQueijo() {
		return tiposDeQueijo;
	}

	public Queijo getQueijo() {
		return queijo;
	}

	public List<Salada> getTiposDeSalada() {
		return tiposDeSalada;
	}

	public Salada getSalada() {
		return salada;
	}
	public List<Molho> getTiposDeMolho() {
		return tiposDeMolho;
	}

	public Molho getMolho() {
		return molho;
	}
	
	public List<Tempero> getTiposDeTempero() {
		return tiposDeTempero;
	}

	public Tempero getTempero() {
		return tempero;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public void cadastrar() {
		PedidoService pedidoService = new PedidoService(this.repositorios.getPedidos());
		pedidoService.salvar(pedido);
		this.pedido = new Pedido();		
		
		String msg = "Pedido efetuado com sucesso!";
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
	}

	public void setLanches(List<Lanche> lanches) {
		this.lanches = lanches;
	}

	public void setTiposDePao(List<TipoPao> tiposDePao) {
		this.tiposDePao = tiposDePao;
	}

	public void setTiposDeQueijo(List<Queijo> tiposDeQueijo) {
		this.tiposDeQueijo = tiposDeQueijo;
	}

	public void setTiposDeRecheio(List<Recheio> tiposDeRecheio) {
		this.tiposDeRecheio = tiposDeRecheio;
	}

	public void setTiposDeSalada(List<Salada> tiposDeSalada) {
		this.tiposDeSalada = tiposDeSalada;
	}

	public void setTiposDeMolho(List<Molho> tiposDeMolho) {
		this.tiposDeMolho = tiposDeMolho;
	}

	public void setTiposDeTempero(List<Tempero> tiposDeTempero) {
		this.tiposDeTempero = tiposDeTempero;
	}

	public void setLanche(Lanche lanche) {
		this.lanche = lanche;
	}

	public void setTipoPao(TipoPao tipoPao) {
		this.tipoPao = tipoPao;
	}

	public void setQueijo(Queijo queijo) {
		this.queijo = queijo;
	}

	public void setRecheio(Recheio recheio) {
		this.recheio = recheio;
	}

	public void setSalada(Salada salada) {
		this.salada = salada;
	}

	public void setMolho(Molho molho) {
		this.molho = molho;
	}

	public void setTempero(Tempero tempero) {
		this.tempero = tempero;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

}
