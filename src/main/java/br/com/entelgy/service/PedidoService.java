package br.com.entelgy.service;

import java.math.BigDecimal;
import java.util.Date;

import br.com.entelgy.model.ItemPedido;
import br.com.entelgy.model.Pedido;
import br.com.entelgy.repository.Pedidos;

public class PedidoService {
	private Pedidos pedidos;
	
	public PedidoService(Pedidos pedidos) {
		this.pedidos = pedidos;
	}
	
	public void salvar(Pedido pedido) {
		BigDecimal valorTotal = BigDecimal.ZERO;
		if(pedido.getItensPedido() != null) {
			for(ItemPedido item : pedido.getItensPedido()) {
				valorTotal = valorTotal.add(item.getValor()) ;
			}
		}
		pedido.setValorTotal(valorTotal);
		
		if(pedido.getCodigo() == null) {
			pedido.setDataPedido(new Date());
		}
		this.pedidos.salvar(pedido);
	}
}




