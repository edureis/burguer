package br.com.entelgy.repository.infra;

import java.util.List;



import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.com.entelgy.model.Pedido;
import br.com.entelgy.repository.Pedidos;

public class ImplPedido implements Pedidos{
	
	private Session session;
	
	public ImplPedido(Session session) {
		this.session = session;
	}

	@Override
	public Pedido salvar(Pedido pedido) {
		return (Pedido) session.merge(pedido);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Pedido> todos() {
		return session.createCriteria(Pedido.class)
				.addOrder(Order.asc("codigo"))
				.list();
	}

	
}



