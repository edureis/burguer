package br.com.entelgy.repository;

import java.util.List;

import br.com.entelgy.model.Pedido;

public interface Pedidos {
	public List<Pedido> todos();
	public Pedido salvar(Pedido pedido);
}
