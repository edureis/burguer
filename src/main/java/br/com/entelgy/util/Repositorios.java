package br.com.entelgy.util;

import java.io.Serializable;

import org.hibernate.Session;

import br.com.entelgy.repository.Pedidos;
import br.com.entelgy.repository.infra.ImplPedido;

public class Repositorios implements Serializable{	

	private Session getSession() {
		return (Session) FacesUtil.getRequestAttribute("session");
	}	
	
	public Pedidos getPedidos() {
		return new ImplPedido(this.getSession());
	}	
}


