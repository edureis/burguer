package br.com.entelgy.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Lanche implements Serializable{
	private Long codigo;	
	private String descricao;
	private BigDecimal valor;
	private TipoPao tipoPao;
	private Queijo queijo;
	private Recheio recheio;
	private Molho molho;
	private Tempero tempero;
	private Salada salada;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	@ManyToOne
	public TipoPao getTipoPao() {
		return tipoPao;
	}
	public void setTipoPao(TipoPao tipoPao) {
		this.tipoPao = tipoPao;
	}
	@ManyToOne
	public Queijo getQueijo() {
		return queijo;
	}
	public void setQueijo(Queijo queijo) {
		this.queijo = queijo;
	}
	@ManyToOne
	public Recheio getRecheio() {
		return recheio;
	}
	public void setRecheio(Recheio recheio) {
		this.recheio = recheio;
	}
	@ManyToOne
	public Molho getMolho() {
		return molho;
	}
	public void setMolho(Molho molho) {
		this.molho = molho;
	}
	@ManyToOne
	public Tempero getTempero() {
		return tempero;
	}
	public void setTempero(Tempero tempero) {
		this.tempero = tempero;
	}
	@ManyToOne
	public Salada getSalada() {
		return salada;
	}
	public void setSalada(Salada salada) {
		this.salada = salada;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lanche other = (Lanche) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
	
}
