package br.com.entelgy.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table
public class ItemPedido {
	
	private Long codigo;	
	private Lanche lanche;
	private Molho molho;
	private Queijo queijo;
	private Recheio recheio;
	private Salada salada;
	private Tempero tempero;
	private TipoPao tipoPao;
	private BigDecimal valor;
	private Pedido pedido;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public Lanche getLanche() {
		return lanche;
	}
	public void setLanche(Lanche lanche) {
		this.lanche = lanche;
	}
	public Molho getMolho() {
		return molho;
	}
	public void setMolho(Molho molho) {
		this.molho = molho;
	}
	public Queijo getQueijo() {
		return queijo;
	}
	public void setQueijo(Queijo queijo) {
		this.queijo = queijo;
	}
	public Recheio getRecheio() {
		return recheio;
	}
	public void setRecheio(Recheio recheio) {
		this.recheio = recheio;
	}
	public Salada getSalada() {
		return salada;
	}
	public void setSalada(Salada salada) {
		this.salada = salada;
	}
	public Tempero getTempero() {
		return tempero;
	}
	public void setTempero(Tempero tempero) {
		this.tempero = tempero;
	}
	public TipoPao getTipoPao() {
		return tipoPao;
	}
	public void setTipoPao(TipoPao tipoPao) {
		this.tipoPao = tipoPao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	@ManyToOne
	@JoinColumn(name = "codigo_pedido")
	public Pedido getPedido() {
		return pedido;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	

}
